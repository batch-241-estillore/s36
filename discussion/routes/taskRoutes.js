//Contains all the endpoints for our application
//We separate the routes such that "index.js" only contains information on the server

const express = require("express");

//Creates a Router instance that functions as a middleware and routing system
//Allow access to http method middlewares that makes it easier to create routes for our application

const router = express.Router();

const taskController = require("../controllers/taskController")

//Routes
//Are responsible for defining the URIs that our client accesses and the corresponding controller function that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller


//Route to GET all the tasks
router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

})

//Route for CREATING a new task
router.post("/", (req,res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for Deleting a task
router.delete("/:id", (req,res) => {

	console.log(req.params);

	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController)
	})
})

//Route for Updating a task
router.put("/:id", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


//Route for Getting specific task
router.get("/:id", (req,res) => {
	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//Updating status
router.put("/:id/complete", (req,res) => {
	taskController.updateTaskStatus(req.params.id).then(resultFromController => res.send(resultFromController));
});

//Use "module.exports" to export the router object to use in the "index.js"
module.exports = router;






