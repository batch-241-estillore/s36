//Controllers contain the functions and business logic of our Express JS application
//Meaning all operations it can do will be placed in this file

//Uses the require directive to allow access the "Task" model which allows us to access methods to perform CRUD operations

const Task = require("../models/task");

//Controller function for GETTING ALL TASKS
//Defines the functions to be used in the "taskRoutes.js" file and export these functions

module.exports.getAllTasks = () => {
	
	//The ".then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman

	return Task.find({}).then(result => {

		return result;

	})
}


module.exports.createTask = (requestBody) => {
	//Creates a task object based on the Mongoose Model "task"
	let newTask = new Task({
		name: requestBody.name
	});
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			return false
		}else{
			return task
		}
	});
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {

			if(error){
				console.log(error)
				return false
			}else{
				return removedTask
			}
	})
}

module.exports.updateTask = (taskId, requestBody) => {

	return Task.findById(taskId).then((result, error) => {

		if(error){
			console.log(error)
			return false
		}

		result.name = requestBody.name;

		return result.save().then((updatedTask, saveErr) =>{

			if(saveErr){
				console.log(saveErr)
			}else{
				return updatedTask
			}
		})
	})
}


//find specific task
module.exports.getOneTask = (taskId) => {
	
	return Task.findById(taskId).then(result => {

		return result;

	})
}

//update status
module.exports.updateTaskStatus = (taskId) => {

	return Task.findById(taskId).then((result, error) => {

		if(error){
			console.log(error)
			return false
		}

		result.status = "complete";

		return result.save().then((updatedTask, saveErr) =>{

			if(saveErr){
				console.log(saveErr)
			}else{
				return updatedTask
			}
		})
	})
}
